Django==1.8.2
MySQL-python==1.2.5
Pillow==2.8.1
Markdown==2.6.2
django-carton==1.2.0
