# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '05/20/15'
from django.core.urlresolvers import reverse
from .forms import ContactForm

def menu_website(request):
    items_menu = {
        'menu_website': [
            {'name': 'Inicio', 'url': reverse('website:home')},
            {'name': 'Qué es comproagro', 'url': reverse('website:whatis')},
            {'name': 'Comprador', 'url': reverse('store:home')},
            {'name': 'Productor', 'url': reverse('website:producer')},
            {'name': 'Registrarme', 'url': reverse('website:register')},
            {'name': 'Mi cuenta', 'url': reverse('website:myaccount')},

        ]
    }
    for item in items_menu['menu_website']:
        if request.path == item['url']:
            item['active'] = True

    return items_menu

def menu_admin(request):
    items_menu = {
        'menu_admin': [
            {'name': 'Pedidos', 'url': reverse('administrator:requests')},
            {'name': 'Compradores', 'url': reverse('administrator:buyers')},
            {'name': 'Productores', 'url': reverse('administrator:producers')},
            {'name': 'Productos', 'url': reverse('administrator:products')},
        ]
    }
    for item in items_menu['menu_admin']:
        if request.path == item['url']:
            item['active'] = True
    return items_menu

def menu_buyer(request):
    items_menu = {
        'menu_buyer': [
            {'name': 'Pedidos', 'url': reverse('buyer:orders')},
            {'name': 'Mis Datos', 'url': reverse('buyer:home')}
        ]
    }
    for item in items_menu['menu_buyer']:
        if request.path == item['url']:
            item['active'] = True
    return items_menu


def contact_form(request):
    contactform = {
        'contact_form': ContactForm()
    }
    return contactform