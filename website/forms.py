# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '23/05/15'
from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(
        label='Nombre*',
        widget=forms.TextInput(attrs={'placeholder': 'Nombre'}),
        max_length=128
    )
    email = forms.EmailField(
        label='Correo Electrónico*',
        widget=forms.TextInput(attrs={'placeholder': 'Correo Electrónico'})
    )
    phone = forms.CharField(
        label='Celular',
        widget=forms.TextInput(attrs={'placeholder': 'Celular'}),
        required=False,
        max_length=128
    )
    message = forms.CharField(
        label='Mensaje*',
        widget=forms.Textarea(attrs={'placeholder': 'Mensaje'})
    )
