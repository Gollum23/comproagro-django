# -*- coding: utf-8 -*-
from __future__ import unicode_literals
"""comproagro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^que-es-comproagro/$', TemplateView.as_view(template_name='whatis_website.html'), name='whatis'),
    url(r'^productor/$', TemplateView.as_view(template_name='producer_website.html'), name='producer'),
    url(r'^registrarme/$', regirster_view, name='register'),
    url(r'^micuenta/$', myaccount_view, name='myaccount'),
    # Ajax request
    url(r'^register-buyer/$', register_buyer_ajax, name='register_buyer'),
    url(r'^register-producer/$', register_producer_ajax, name='register_producer'),
    url(r'^send-contact/$', send_contact_ajax, name='send_contact'),
]
