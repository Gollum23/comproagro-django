# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect


from administrator.forms import EmailAuthenticationForm, UserForm, ProducerProfileForm, BuyerProfileForm
from administrator.models import UserModel, Buyer, Producer
from .forms import ContactForm


def home_view(request):
    formuser = UserForm(prefix='user', label_suffix='*')
    formbuyer = BuyerProfileForm(prefix='buyer', label_suffix='')
    formproducer = ProducerProfileForm(prefix='producer', label_suffix='')

    ctx = {
        'formuser': formuser,
        'formbuyer': formbuyer,
        'formproducer': formproducer
    }

    return render(request, 'home_website.html', ctx)


def regirster_view(request):
    formuser = UserForm(prefix='user', label_suffix='')
    formbuyer = BuyerProfileForm(prefix='buyer', label_suffix='')
    formproducer = ProducerProfileForm(prefix='producer', label_suffix='')

    ctx = {
        'formuser': formuser,
        'formbuyer': formbuyer,
        'formproducer': formproducer
    }

    return render(request, 'register_website.html', ctx)


def myaccount_view(request):
    user = request.user
    # form = None
    if user.is_authenticated():
        if user.is_superuser:
            return HttpResponseRedirect(reverse_lazy('administrator:home'))
        elif user.profile == 'comprador':
            try:
                cart = request.session['CART']
            except KeyError:
                cart = None
            if cart:
                return HttpResponseRedirect(reverse_lazy('store:show_car'))
            else:
                return HttpResponseRedirect(reverse_lazy('buyer:orders'))
        else:
            return HttpResponseRedirect(reverse_lazy('website:home'))
    else:
        form = EmailAuthenticationForm(request.POST or None)
        if form.is_valid():
            login(request, form.get_user())
            return HttpResponseRedirect(request.path)

    ctx = {
        'form': form
    }
    return render(request, 'my_account.html', ctx)


def logout_view(request):
    logout(request)
    return redirect(reverse_lazy('website:home'))


# AJAX Requests
def register_buyer_ajax(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            usercache = UserModel.objects.get(email=request.POST.get('user-email'))
        except ObjectDoesNotExist:
            usercache = None
        if not usercache:
            user = UserModel(
                email=request.POST['user-email'],
                password=make_password(request.POST['user-password']),
                profile='comprador'
            )
            user.save()
            buyer = Buyer(
                user=user,
                name=request.POST['buyer-name'],
                identification=request.POST['buyer-identification'],
                address=request.POST['buyer-address'],
                phone=request.POST['buyer-phone'],
                city=request.POST['buyer-city'],
                email_notifications=request.POST['buyer-email_notifications'],
                product_wanted=request.POST['buyer-product_wanted'],
                quantity_wanted=request.POST['buyer-quantity_wanted']
            )
            buyer.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=302)
    return HttpResponse(status=403)


def register_producer_ajax(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            usercache = UserModel.objects.get(email=request.POST.get('user-email'))
        except ObjectDoesNotExist:
            usercache = None
        if not usercache:
            user = UserModel(
                email=request.POST['user-email'],
                password=make_password(request.POST['user-password']),
                profile='productor'
            )
            user.save()
            producer = Producer(
                user=user,
                name=request.POST['producer-name'],
                identification=request.POST['producer-identification'],
                address=request.POST['producer-address'],
                phone=request.POST['producer-phone'],
                city=request.POST['producer-city'],
                email_notifications=request.POST['producer-email_notifications'],
                produced_products=request.POST['producer-produced_products'],
                produced_quantity=request.POST['producer-produced_quantity'],
                production_time=request.POST['producer-production_time']
            )
            producer.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=302)
    return HttpResponse(status=403)


def send_contact_ajax(request):
    if request.is_ajax():
        form = ContactForm(request.POST)
        if form.is_valid():
            contact_data = form.cleaned_data
            full_message = 'Nombre: ' + contact_data['name'] + '<br />' + 'correo: ' \
                           '' + contact_data['email'] + '<br />' + 'celular: ' \
                           '' + contact_data.get('phone', 'No registra') + '<br />' \
                           '' + contact_data['message']

            send_mail(
                'Contato desde la web',                 # Subject
                full_message,                           # Message
                contact_data['email'],                  # From Email
                settings.EMAIL_RECIPENT_LIST_CONTACT,   # Destinataries
                html_message=full_message               # Message html format
            )
            return HttpResponse(status=200)
    return HttpResponse(status=403)
