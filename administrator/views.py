# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import ProtectedError
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string

from .forms import ProducerProfileForm, UserForm, UserFormEdit, BuyerProfileForm, MyPasswordChangeForm, DeleteForm
from .models import Producer, Buyer, UserModel
from store.forms import ProductForm, DeleteProductForm
from store.models import Order, Product


# Vista Creación de pedido
def order_add_view(request):
    pass


# Vista Lista de pedidos
@login_required(login_url=reverse_lazy('website:myaccount'))
def order_list_view(request):
    list_orders = modelformset_factory(Order, extra=0, fields=('shipping_date', 'state',))
    if request.is_ajax():
        formset = list_orders(request.POST or None, queryset=Order.objects.all())
        if formset.is_valid() and formset.has_changed():
            for f in formset:
                if f.has_changed():
                    f.save()
                    if 'state' in f.changed_data:
                        subject = 'El estado del pedido ' + f.instance.code + ' cambio a ' + f.instance.state
                        msg_txt = render_to_string('email/change_state.txt', {'order': f.instance})
                        msg_html = render_to_string('email/change_state.html', {'order': f.instance})
                        list_recipents = settings.EMAIL_RECIPENT_LIST_CONTACT
                        list_recipents.append(f.instance.buyer.user.email)
                        send_mail(
                            subject,
                            msg_txt,
                            'info@comproagro.com',
                            list_recipents,
                            html_message=msg_html
                        )
                        list_recipents.pop()
                        mymessage = 'El pedido %s cambio de estado a %s' % (f.instance.code, f.instance.state)

                    if 'shipping_date' in f.changed_data:
                        mymessage = 'La fecha de entrega del pedido %s cambio a %s' % (f.instance.code,
                                                                               f.instance.shipping_date)
                    data = {'message': mymessage}
                    return JsonResponse(data, safe=False)

    ctx = {
        'list_orders': list_orders,
    }
    return render(request, 'request-list.html', ctx)


# Vista Update de pedidos
def order_update_view(request):
    pass


# Vista Borrado de pedido
# TODO: Pendiente vista borrado de pedido


@login_required(login_url=reverse_lazy('website:myaccount'))
def detail_order_view(request, req):
    order = Order.objects.get(pk=req)
    ctx = {
        'order': order
    }
    return render(request, 'request-detail.html', ctx)


# Vista Creación de comprador
@login_required(login_url=reverse_lazy('website:myaccount'))
def buyer_add_view(request):
    form = UserForm(request.POST or None, prefix='user', label_suffix='')
    formp = BuyerProfileForm(request.POST or None, prefix='profile', label_suffix='')
    if form.is_valid():
        user = form.save(commit=False)
        user.profile = 'comprador'
        user.save()
        if formp.is_valid():
            profile = formp.save(commit=False)
            profile.user_buyer = user
            profile.save()
            return HttpResponseRedirect(reverse_lazy('administrator:buyers'))

    ctx = {
        'form': form,
        'formp': formp
    }
    return render(request, 'buyer-add.html', ctx)


# Vista Lista de compradores
@login_required(login_url=reverse_lazy('website:myaccount'))
def buyer_list_view(request):
    list_buyers = Buyer.objects.all()
    ctx = {
        'list_buyers': list_buyers
    }
    return render(request, 'buyers-list.html', ctx)


# Vista Update de comprador
@login_required(login_url=reverse_lazy('website:myaccount'))
def buyer_update_view(request, buyerid):
    buyer = Buyer.objects.get(pk=buyerid)
    form = UserForm(request.POST or None, instance=buyer.user)
    formp = BuyerProfileForm(request.POST or None, instance=buyer)
    if form.is_valid():
        user = form.save(commit=False)
        user.save()
        if formp.is_valid():
            profile = formp.save(commit=False)
            profile.user_buyer = user
            profile.save()
            return HttpResponseRedirect(reverse_lazy('administrator:buyers'))

    ctx = {
        'form': form,
        'formp': formp,
        'buyer': buyer
    }
    return render(request, 'buyer-detail.html', ctx)


# Vista Borrado de comprador
@login_required(login_url=reverse_lazy('website:myaccount'))
def buyer_delete_view(request, user):
    form = DeleteForm(request.POST or None)
    user = UserModel.objects.get(pk=user)
    buyer = Buyer.objects.get(user=user.id)
    if form.is_valid():
        try:
            buyer.delete()
            user.delete()
            return HttpResponseRedirect(reverse_lazy('administrator:buyers'))
        except ProtectedError:
            messages.info(request, 'El comprador no se puede borrar porque se encuentra relacionado en los pedidos')
            return HttpResponseRedirect(reverse_lazy('administrator:buyers'))
    ctx = {
        'form': form,
        'buyer': buyer
    }
    return render(request, 'buyer-delete.html', ctx)


# Vista Creación de productor
@login_required(login_url=reverse_lazy('website:myaccount'))
def producer_add_view(request):
    form = UserForm(request.POST or None)
    formp = ProducerProfileForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        user.profile = 'productor'
        user.save()
        if formp.is_valid():
            profile = formp.save(commit=False)
            profile.user_producer = user
            profile.save()
            return HttpResponseRedirect(reverse_lazy('administrator:producers'))
    ctx = {
        'form': form,
        'formp': formp
    }
    return render(request, 'producer-add.html', ctx)


# Vista Lista de productores
@login_required(login_url=reverse_lazy('website:myaccount'))
def producer_list_view(request):
    list_producer = Producer.objects.all()
    ctx = {
        'list_producer': list_producer
    }
    return render(request, 'producers-list.html', ctx)


# Vista Update de productores
@login_required(login_url=reverse_lazy('website:myaccount'))
def producer_update_view(request, prod):
    producer = Producer.objects.get(pk=prod)
    form = UserFormEdit(request.POST or None, instance=producer.user)
    formp = ProducerProfileForm(request.POST or None, instance=producer)
    if form.is_valid():
        user = form.save(commit=False)
        user.profile = 'producer'
        user.save()
        if formp.is_valid():
            profile = formp.save(commit=False)
            profile.user_producer = user
            profile.save()
            return HttpResponseRedirect(reverse_lazy('administrator:producers'))
    ctx = {
        'form': form,
        'formp': formp
    }
    return render(request, 'producer-add.html', ctx)


# Vista Borrado de productores
@login_required(login_url=reverse_lazy('website:myaccount'))
def producer_delete_view(request, user):
    form = DeleteForm(request.POST or None)
    user = UserModel.objects.get(pk=user)
    producer = Producer.objects.get(user=user.id)
    if form.is_valid():
        try:
            producer.delete()
            user.delete()
            return HttpResponseRedirect(reverse_lazy('administrator:producers'))
        except ProtectedError:
            messages.info(request, 'El productor no se puede borrar porque se encuentra relacionado en los pedidos')
            return HttpResponseRedirect(reverse_lazy('administrator:producers'))
    ctx = {
        'form': form,
        'producer': producer
    }
    return render(request, 'producer-delete.html', ctx)

# Vista Creación de productos
@login_required(login_url=reverse_lazy('website:myaccount'))
def product_add_view(request):
    form = ProductForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect(reverse_lazy('administrator:products'))
    ctx = {
        'form': form
    }
    return render(request, 'product-add.html', ctx)


# Vista lista de productos
@login_required(login_url=reverse_lazy('website:myaccount'))
def product_list_view(request):
    list_product = Product.objects.all()
    ctx = {
        'list_product': list_product
    }
    return render(request, 'products-list.html', ctx)


# Vista update productos
@login_required(login_url=reverse_lazy('website:myaccount'))
def product_update_view(request, cod):
    producto = Product.objects.get(pk=cod)
    form = ProductForm(request.POST or None, request.FILES or None, instance=producto)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse_lazy('administrator:products'))
    ctx = {
        'form': form
    }
    return render(request, 'product-add.html', ctx)

# Vista delete producto
def product_delete_view(request, cod):
    form = DeleteProductForm(request.POST or None)
    producto = Product.objects.get(pk=cod)
    if form.is_valid():
        try:
            producto.delete()
            return HttpResponseRedirect(reverse_lazy('administrator:products'))
        except ProtectedError:
            messages.info(request, 'El producto no se puede borrar porque se encuentra relacionado en los pedidos')
            return HttpResponseRedirect(reverse_lazy('administrator:products'))
    ctx = {
        'form': form,
        'product': producto
    }
    return render(request, 'product-delete.html', ctx)



@login_required(login_url=reverse_lazy('website:myaccount'))
def change_password_view(request):
    form = MyPasswordChangeForm(request.user, request.POST or None)
    if form.is_valid():
        form.save()
    ctx = {
        'form': form
    }
    return render(request, 'change_password.html', ctx)
