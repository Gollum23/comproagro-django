# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import administrator.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
                ('email', models.EmailField(unique=True, max_length=255)),
                ('profile', models.CharField(max_length=255, blank=True)),
                ('is_superuser', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'user',
                'verbose_name': 'Usuario',
                'verbose_name_plural': 'Usuarios',
            },
            managers=[
                ('objects', administrator.models.UserManagerCustom()),
            ],
        ),
        migrations.CreateModel(
            name='Buyer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('identification', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('email_notifications', models.BooleanField(default=False)),
                ('product_wanted', models.CharField(max_length=255, null=True, blank=True)),
                ('quantity_wanted', models.CharField(max_length=255, null=True, blank=True)),
                ('contact_name', models.CharField(max_length=255, null=True, blank=True)),
                ('contact_email', models.EmailField(max_length=255, null=True, blank=True)),
                ('contact_phone', models.CharField(max_length=255, null=True, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'buyer',
                'verbose_name': 'Comprador',
                'verbose_name_plural': 'Compradores',
            },
        ),
        migrations.CreateModel(
            name='Producer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('identification', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('email_notifications', models.BooleanField(default=False)),
                ('produced_products', models.CharField(max_length=255, null=True, blank=True)),
                ('produced_quantity', models.CharField(max_length=255, null=True, blank=True)),
                ('production_time', models.CharField(max_length=255, null=True, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'producer',
                'verbose_name': 'Productor',
                'verbose_name_plural': 'Productores',
            },
        ),
    ]
