# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible


class TimeStampModel(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )
    modified = models.DateTimeField(
        auto_now=True
    )

    class Meta:
        abstract = True

class UserManagerCustom(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True, **extra_fields)


@python_2_unicode_compatible
class UserModel(AbstractBaseUser, TimeStampModel):
    email = models.EmailField(
        max_length=255,
        unique=True,
        verbose_name='Correo electrónico'
    )
    profile = models.CharField(
        max_length=255,
        blank=True
    )
    is_superuser = models.BooleanField(
        default=False
    )

    objects = UserManagerCustom()

    REQUIRED_FIELDS = ['password']
    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
        db_table = 'user'

    def __str__(self):
        return '%s' % self.email


@python_2_unicode_compatible
class Buyer(TimeStampModel):
    user = models.OneToOneField(
        UserModel,
        on_delete=models.PROTECT
    )
    name = models.CharField(
        max_length=255
    )
    identification = models.CharField(
        max_length=255
    )
    address = models.CharField(
        max_length=255
    )
    phone = models.CharField(
        max_length=255
    )
    city = models.CharField(
        max_length=255
    )
    email_notifications = models.BooleanField(
        default=False
    )
    product_wanted = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    quantity_wanted = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    contact_name = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    contact_email = models.EmailField(
        max_length=255,
        blank=True,
        null=True
    )
    contact_phone = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Comprador'
        verbose_name_plural = 'Compradores'
        db_table = 'buyer'

    def __str__(self):
        return '%s' % self.name_buyer


@python_2_unicode_compatible
class Producer(TimeStampModel):
    user = models.OneToOneField(
        UserModel,
        on_delete=models.PROTECT
    )
    name = models.CharField(
        max_length=255
    )
    identification = models.CharField(
        max_length=255
    )
    address = models.CharField(
        max_length=255
    )
    phone = models.CharField(
        max_length=255
    )
    city = models.CharField(
        max_length=255
    )
    email_notifications = models.BooleanField(
        default=False
    )
    produced_products = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    produced_quantity = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    production_time = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Productor'
        verbose_name_plural = 'Productores'
        db_table = 'producer'

    def __str__(self):
        return '%s' % self.name_producer



