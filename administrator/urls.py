# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, RedirectView

from .views import *

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('administrator:requests')), name='home'),
    url(r'^pedidos/$', order_list_view, name='requests'),
    url(r'^pedido-detalle/(?P<req>\d+)/$', detail_order_view, name='request__detail'),
    url(r'^compradores/$', buyer_list_view, name='buyers'),
    url(r'^compradores/agregar/$', buyer_add_view, name='buyers__add'),
    url(r'^compradores/editar/(?P<buyerid>\d+)/$', buyer_update_view, name='buyers__edit'),
    url(r'^compradores/borrar/(?P<user>\d+)/$', buyer_delete_view, name='buyers__delete'),
    url(r'^productores/$', producer_list_view, name='producers'),
    url(r'^productores/agregar/$', producer_add_view, name='producer__add'),
    url(r'^productores/editar/(?P<prod>\d+)/$', producer_update_view, name='producer__edit'),
    url(r'^productores/borrar/(?P<user>\d+)/$', producer_delete_view, name='producer__delete'),
    url(r'^productos/$', product_list_view, name='products'),
    url(r'^productos/agregar/$', product_add_view, name='product__add'),
    url(r'^productos/editar/(?P<cod>\d+)/$', product_update_view, name='product__edit'),
    url(r'^productos/borrar/(?P<cod>\d+)/$', product_delete_view, name='product__delete'),
    url(r'^cambio-contrasena/$', change_password_view, name='change_password')
]
