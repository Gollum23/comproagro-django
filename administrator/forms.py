# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from collections import OrderedDict
from django import forms
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth import authenticate

from .models import Producer, Buyer, UserModel


class UserForm(forms.ModelForm):

    class Meta:
        model = UserModel
        fields = ('password', 'email', )
        widgets = {
            'email': forms.TextInput(attrs={'placeholder': 'Correo electrónico', 'required': 'true', 'type': 'email'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Contraseña', 'required': 'true'}),
        }
        label = {
            'email': 'Correo electrónico*',
            'password': 'Contraseña*',
        }

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class EmailAuthenticationForm(forms.Form):
    email = forms.EmailField(
        label='Correo',
        widget=forms.TextInput(attrs={'placeholder': 'Correo electrónico'})
    )
    password = forms.CharField(
        label='Contraseña',
        widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña'})
    )

    def __init__(self, *args, **kwargs):
        self.user_cache = None
        super(EmailAuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        self.user_cache = authenticate(email=email, password=password)

        if self.user_cache is None:
            raise forms.ValidationError('Usuario y/o contraseña incorrecto')
        return self.cleaned_data

    def get_user(self):
        return self.user_cache


class UserFormEdit(forms.ModelForm):

    class Meta:
        model = UserModel
        fields = ('email',)

    def clean_password(self):
        return self.initial['password']

class MyPasswordChangeForm(forms.Form):
    """
    A form that lets a user change their password by entering their old
    password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': "La contraseña que ha insertado es incorrecta, Por favor intente de nuevo"
    })
    old_password = forms.CharField(
        label="Contraseña antigua",
        widget=forms.PasswordInput
    )
    new_password = forms.CharField(
        label="Nueva contraseña",
        widget=forms.PasswordInput
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(MyPasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password'])
        if commit:
            self.user.save()
        return self.user


class ProducerProfileForm(forms.ModelForm):

    produced_products = forms.CharField(
        required=False,
        label='Producto(s)*',
        widget=forms.TextInput(attrs={'placeholder': 'Producto(s)', 'required': 'true'})
    )
    produced_quantity = forms.CharField(
        required=False,
        label='Cantidad aproximada de producción*',
        widget=forms.TextInput(attrs={'placeholder': 'Cantidad aproximada de producción', 'required': 'true'})
    )
    production_time = forms.CharField(
        required=False,
        label='Tiempo estimado entre siembra y recolección',
        widget=forms.TextInput(
            attrs={'placeholder': 'Tiempo estimado entre siembra y recolección'}
        )

    )

    class Meta:
        model = Producer
        exclude = ('user',)
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Nombre', 'required': 'true'}),
            'identification': forms.TextInput(attrs={'placeholder': 'Identificación', 'required': 'true'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Celular', 'required': 'true'}),
            'address': forms.TextInput(attrs={'placeholder': 'Dirección', 'required': 'true'}),
            'city': forms.TextInput(attrs={'placeholder': 'Lugar de producción', 'required': 'true'}),
            'email_notifications': forms.CheckboxInput(attrs={'class': 'black', 'required': 'true'})
        }
        labels = {
            'name': 'Nombre*',
            'identification': 'Identificación*',
            'phone': 'Celular*',
            'address': 'Dirección*',
            'city': 'Lugar de producción*',
            'email_notifications': 'Recibir notificaciones en mi correo'
        }


class DeleteForm(forms.Form):
    user = forms.CharField(
        max_length=50
    )


class BuyerProfileForm(forms.ModelForm):

    class Meta:
        model = Buyer
        exclude = ('user', )
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Nombre', 'required': 'true'}),
            'identification': forms.TextInput(attrs={'placeholder': 'Identificación', 'required': 'true'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Teléfono', 'required': 'true'}),
            'product_wanted': forms.TextInput(attrs={'placeholder': 'Productos de interes'}),
            'quantity_wanted': forms.TextInput(attrs={'placeholder': 'Cantidad aproximada'}),
            'address': forms.TextInput(attrs={'placeholder': 'Dirección', 'required': 'true'}),
            'city': forms.TextInput(attrs={'placeholder': 'Ciudad', 'required': 'true'}),
            'contact_name': forms.TextInput(attrs={'placeholder': 'Persona de contacto'}),
            'contact_email': forms.TextInput(attrs={'placeholder': 'Email de contacto'}),
            'contact_phone': forms.TextInput(attrs={'placeholder': 'Teléfono de contacto'}),
            'email_notifications': forms.CheckboxInput(attrs={'class': 'black'})
        }
        labels = {
            'name': 'Nombre*',
            'identification': 'Identificación*',
            'phone': 'Celular*',
            'product_wanted': 'Productos de interes',
            'quantity_wanted': 'Cantidad aproximada',
            'address': 'Dirección*',
            'city': 'Ciudad*',
            'contact_name': 'Persona de contacto',
            'contact_email': 'Correo eletrónico contacto',
            'contact_phone': 'Teléfono de contacto',
            'email_notifications': 'Recibir notificaciones en mi correo'
        }


class BuyerEditProfileForm(forms.ModelForm):

    class Meta:
        model = Buyer
        exclude = ('user', 'email_notifications',)
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Nombre'}),
            'identification': forms.TextInput(attrs={'placeholder': 'Identificación'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Teléfono'}),
            'product_wanted': forms.TextInput(attrs={'placeholder': 'Productos de interes'}),
            'quantity_wanted': forms.TextInput(attrs={'placeholder': 'Cantidad aproximada'}),
            'address': forms.TextInput(attrs={'placeholder': 'Dirección'}),
            'city': forms.TextInput(attrs={'placeholder': 'Ciudad'}),
            'contact_name': forms.TextInput(attrs={'placeholder': 'Persona de contacto'}),
            'contact_email': forms.TextInput(attrs={'placeholder': 'Email de contacto'}),
            'contact_phone': forms.TextInput(attrs={'placeholder': 'Teléfono de contacto'}),
        }
        labels = {
            'name': 'Nombre',
            'identification': 'Identificación',
            'phone': 'Teléfono',
            'product_wanted': 'Productos de interes',
            'quantity_wanted': 'Cantidad aproximada',
            'address': 'Dirección',
            'city': 'Ciudad',
            'contact_name': 'Persona de contacto',
            'contact_email': 'Correo eletrónico contacto',
            'contact_phone': 'Teléfono de contacto',
        }
