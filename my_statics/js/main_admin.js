/**
 * Created by gollum23 on 20/05/15.
 */
(function() {
    'use strict';
    $(document).ready(iniciar);

    function iniciar() {
        $('.jimage').on('change', showImage);
        $('#list-orders' ).find('input').on('change', saveFormListOrdersDate);
        $('#list-orders' ).find('select').on('change', saveFormListOrdersState);
        $('.main-content__header__user--app' ).on('click', toogleMenuAdmin);
    }

    function adjust(img, input) {


        //
        console.log(output.width)
    }

    function showImage(e) {
        var output = document.getElementById('product-preview');
        var input = e.target;
        var file = (input.files[0] ? input.files[0]:null);
        if (file == null) {
            return;
        }
        self.file = file;
        var reader = new FileReader();
        reader.onload = function (e) {
            output.src = reader.result;
            var MAX_W = $('.form__item.relative').width();
            var MAX_H = 200;
            var os = output.width / output.height;
            var ts = MAX_W / MAX_H;
            console.log(os, ts);
            console.log($);
            if((os>=1 && ts> 1) || (os<=1 && ts>1)) {
                output.width = MAX_H*os;
                output.height = MAX_H
            }
            else if((os>1 && ts<1) || (os<1 && ts<=1)) {
                output.height = MAX_H;
                output.width = output.height*os;
            }
            else if(os==1 && ts ==1) {
                output.width = MAX_W;
                output.height = MAX_H;
            }
        }
        reader.readAsDataURL(self.file)
    }

    function saveFormListOrdersDate(e){
        $.post('/administrador/pedidos/', $('#list-orders' ).serializeArray() )
                .done(function(data) {
                     $('.message--info' ).html (data.message ).fadeIn().delay(3000).fadeOut();
                })
    }

    function saveFormListOrdersState(e){
        $.post('/administrador/pedidos/', $('#list-orders' ).serializeArray() )
                .done(function(data) {
                    $('.message--info' ).html (data.message ).fadeIn().delay(3000).fadeOut();
                })
    }

    function toogleMenuAdmin(e){
        var $menu = $('.main-content__header__menu');
        if ($menu.hasClass('main-content__header__menu--hidden')) {
            $menu.fadeIn();
            $menu.removeClass('main-content__header__menu--hidden')
        }
        else {
            $menu.fadeOut();
            $menu.addClass('main-content__header__menu--hidden')
        }
    }


})();