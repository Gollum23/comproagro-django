/**
 * @author cAmilo
 */
+function($) {
    var JimageObject = function(element, options, type, parent) {
        var elem = $(element);
        var self = this;
        self.parent=parent;
        self.elem = elem;
        self.preview = null;
        self.data = null;
        self.file = null;
        self.clear = function() {
            var self = this;
            self.preview = null;
            self.data = null;
            self.file = null;
            self.error = false;
            self.elem.val('');
        };
        var defaults={
            responsive:true,
            width:100,
            height:100,
            previewContainer:undefined
        };
        self.options = $.extend(defaults, options);
        self.onload = self.options.onload || function() {};
        self.center=function(){};
        self.error = false;

        var transform=function(img) {
            var canvas = document.createElement("canvas");
            var MAX_WIDTH = self.options.width || 100;
            var MAX_HEIGHT = self.options.height || 100;
            if (self.parent !== undefined) {
                MAX_WIDTH = self.parent.width();
                MAX_HEIGHT = self.parent.height();
            }
            var width = img.width;
            var height = img.height;

            var wratio = MAX_WIDTH / width;
            var hratio = MAX_HEIGHT / height;
            var os = width / height;
            var ts = MAX_WIDTH/MAX_HEIGHT;
            var sy = 0;
            var sx = 0;
            if((os>=1 && ts>1) || (os<=1 && ts>1)){
                width=MAX_WIDTH;
                height=width/os;
                sy=(height-MAX_HEIGHT)/-2;
                sx=0;
            }else if((os>1 && ts<=1) || (os<1 && ts<=1)){
                height=MAX_HEIGHT;
                width=height*os;
                sx=(width-MAX_WIDTH)/-2;
                sy=0;
            }else if(os==1 && ts==1){
                width=MAX_WIDTH;
                height=MAX_HEIGHT;
            }
            canvas.width = MAX_WIDTH;
            canvas.height = MAX_HEIGHT;
            ctx = canvas.getContext("2d");
            var timg=document.createElement("img");
            timg.onload=function(){
                ctx.drawImage(timg, 0, 0, timg.width, timg.height, sx, sy, width, height);
                self.data = canvas.toDataURL("image/jpg");
                self.preview = createPreview(self.parent,timg.src);
                self.onload(elem);                
            };
            timg.src=img;
        };
        
        var inputWorker = function(e) {
            var input = e.target;
            var file = (input.files && input.files[0] ? input.files[0] : null);
            if (file == null)
                return;
            self.file = file;
            var reader = new FileReader();
            reader.onload = function(e){
                transform(e.target.result);
            };
            reader.readAsDataURL(self.file);
        };
         elem.data = self;
        if(type=="input"){
            elem.attr("accept", ".jpg"); // ACCEPTS ONLY JPG
            elem.on('change', inputWorker);
        }
    };
    var old=$.fn.jimage;
    var template = '<div class="jimage-uploader">\n<label for="$[ID];"><span class="glyphicon glyphicon-upload"> </span><span> Imagén de Fondo</span></label>\n<div class="jimage-preview">$[PREVIEW];</div>\n<div class="jimage-actions">\n<p class="jimage-change"><label for="$[ID];"><span class="glyphicon glyphicon-upload"> </span> Cambiar</label></p>\n<p class="jimage-delete"><span><span class="glyphicon glyphicon-trash"> </span> Eliminar</span></p>\n</div>\n</div>';
    function internalEvents(elem,parent){
        parent.find(".jimage-change").click(function(ev) {
            $(elem).trigger("change");
        });
        parent.find(".jimage-delete").click(function(ev) {
            ev.preventDefault();
            $(elem).jimage('clear');
            clearPreview(parent);
            $(elem).trigger("delete");
        });
    }
    function init(elem){
        var id=$(elem).attr("id"), elemsrc=$(elem).clone().wrap('<div>').parent().html();
        var container=template.replace("$[ID];",id).replace("$[ID];",id);
        var preview=$(elem).attr("data-preview");
        if(preview=="" || typeof preview=="undefined"){
            preview="";
        }else{
            preview='<img src="' + preview + '" />';
        }
        container=container.replace("$[PREVIEW];",preview);
        var width=$(elem).width();
        var height=$(elem).height();
        $(elem).wrap(container);
        var parent=$("#" + id).parent();
        if(preview!=""){
            parent.find(".jimage-preview").addClass('show');
            parent.find(".jimage-actions").addClass('show');
        }
        parent.width(width);
        parent.height(height);
        
        parent.closest("label").fitText(1.1);
        internalEvents(elem,parent);
        return parent;
    }
    function createPreview(elem,data){
        $(elem).find(".jimage-preview").addClass('show');
        $(elem).find(".jimage-actions").addClass('show');
        $(elem).find(".jimage-actions>p").fitText(1.1,{minFontSize:13});
        return $(elem).find(".jimage-preview").html('<img src="' + data + '" />');
    }
    function clearPreview(elem){
        $(elem).find(".jimage-preview").html('');
        $(elem).find(".jimage-preview").removeClass('show');
        $(elem).find(".jimage-actions").removeClass('show');
    }
    $.fn.jimage = function(options) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('jimage');
            if (typeof data =="undefined" || !data) {
                if ($this.prop("tagName") == "INPUT" && $this.prop("type") == "file") {
                    var parent=init(this);
                    $this.data('jimage', ( data = new JimageObject(this, options, "input", parent)));
                }
            }
            if(typeof options == "string") data[options]();
        });
    };
    $.fn.jimage.noConflict=function() {
        $.fn.jimage=old;
        return this;
    };
    $(document).ready(function () {
        var $input = $('input.jimage[type=file]'), count = $input.attr('type') != null ? $input.length : 0;
        if (count > 0) {
            $input.jimage();
        }
    });
}(jQuery);