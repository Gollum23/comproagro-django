/**
 * Created by gollum23 on 04/05/15.
 */
(function() {
    'use strict';
    var $ = window.jQuery;
    var isPlaying = false;
    var $stopBtn = $('.stop-head');
    var hideBgModal = true;
    $(document).ready(init);

    function init() {
        $('.video--large, .video--short').fitVids();
        $('.contact__link').on('click', toogleMenu);
        $('.want-buy, .want-sale').on('click', showForm);
        $('.modal').on('click', hideForm);
        $('.play-head').on('click', showVideo);
        $(window).scroll(showme);
        $('#filter_store, #id_paginate_by, #id_order_by').on('change', filter);
        $('#make_request' ).on('click', makeRequest);

    }

    function toogleMenu(e) {
        //e.preventDefault();
        if($(this).hasClass('contact__link--hide')) {
            $('.contact').animate({width: 610}, {duration:500, queue:false});
            $(this).removeClass('contact__link--hide').addClass('contact__link--show');
            $('.modal').fadeIn({duration:500, queue:false});
            $('#id_contact_submit').on('click', sendContact);
        }
        else {
            $('.contact').animate({width: 40}, {duration:500, queue:false});
            $(this).removeClass('contact__link--show').addClass('contact__link--hide');
            if (hideBgModal) {
                hideModal()
            }
            $('#id_contact_submit').off('click');
        }
    }

    function showForm(){
        $('.modal').fadeIn({duration:500, queue:false});
        if($(this).hasClass('want-buy')){
            $('.buyer-form').fadeIn({duration:500, queue:false}).addClass('visible-form');
            var $form = $('#registerBuyerForm');
            $form.submit(function(e) {
                e.preventDefault();
                registerBuyer(e);
            });
        }
        if($(this).hasClass('want-sale')){
            $('.producer-form').fadeIn({duration:500, queue:false}).addClass('visible-form');
            var $form = $('#registerProducerForm');
            $form.submit(function(e) {
                e.preventDefault();
                registerProducer(e);
            })
        }
    }
    function hideForm(){
        if (hideBgModal) {
            hideModal()
        }

        if($('.buyer-form').hasClass('visible-form')){
            $('.buyer-form').fadeOut({duration:500, queue:false}).removeClass('visible-form' ).find('form' )[0].reset();
        }
        if($('.producer-form').hasClass('visible-form')){
            $('.producer-form').fadeOut({duration:500, queue:false}).removeClass('visible-form' ).find('form' )[0].reset();
        }
        if($('.contact__link').hasClass('contact__link--show')){
            $('.contact').animate({width: 40}, {duration:500, queue:false});
            $('.contact__link').removeClass('contact__link--show').addClass('contact__link--hide' ).find('form' )[0].reset();
        }
    }

    function hideModal(){
        $('.modal').fadeOut({duration:500, queue:false});
    }

    function showme(){
        $('.hideme').each( function(i){
            var bottom_of_object = $(this).position().top + $(this).outerHeight()/2 ;
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
                $(this).animate({'opacity':'1'},500);
            }
        });
    }

    function showVideo(e) {
        e.preventDefault();
        var mainvid='<iframe frameborder="0" height="100%" width="100%" src="https://youtube.com/embed/L-2UnPiUb2c?autoplay=1&controls=0&showinfo=0&autohide=1&modestbranding=1&rel=0" wmode="Opaque">ComproAgro Apps.co</iframe>';
        $('.head__content, .navigation').fadeOut();
        $('.video-container').fadeIn().append(mainvid);
        $('.contact').css({'z-index':-11});
        setTimeout(playMain, 2500);
        setTimeout(stopMain, 114000);
    }

    function playMain() {
        $stopBtn.on('click', function() {
            stopMain();
        });
        $stopBtn.fadeIn().animate({opacity:0.5, bottom:67}, 700);
        $stopBtn.hover(function() {
            $(this).animate({opacity:1}, 400);
        }, function() {
            $(this).animate({opacity:0.5}, 400);
        });
        isPlaying = true;
    }

    function stopMain() {
        $stopBtn.off('click');
        $stopBtn.animate({opacity:0, bottom:-100}, 700).fadeOut();
        $('.video-container').fadeOut();
        $('.video-container iframe').remove();
        $('.head__content, .navigation').fadeIn();
        $('.contact').css({'z-index':''});
        isPlaying = false;
    }

    function registerBuyer(e) {
        e.preventDefault();
        var $form = $('#registerBuyerForm');
        $.post('/register-buyer/', $form.serializeArray() )
            .done(function(data) {
                hideBgModal = false;
                hideForm();
                showPopupRegister();
            })
            .fail(function(data) {
                hideBgModal =false;
                hideForm();
                if (data.status == 302) {
                    showPopupRegisterExist()
                }
                if (data.status == 403 || data.status == 500) {
                    showPopupRegisterError();
                }

            })

    }
    function registerProducer(e) {
        e.preventDefault();
        var $form = $('#registerProducerForm');
        $.post('/register-producer/', $form.serializeArray())
                .done(function(data) {
                    hideBgModal = false;
                    hideForm();
                    showPopupRegister();
                })
                .fail(function(data) {
                    hideBgModal =false;
                    hideForm();
                    if (data.status == 302) {
                        showPopupRegisterExist()
                    }
                    if (data.status == 403 || data.status == 500) {
                        showPopupRegisterError();
                    }

                })
    }

    function sendContact(e) {
        e.preventDefault();
        var $form = $('#contactForm');
        $.post('/send-contact/', $form.serializeArray())
                .done(function(data) {

                    hideBgModal = false;
                    toogleMenu();
                    showPopupContact();
                })
    }

    function showPopupRegister(e) {
        $('.register-popup' ).fadeIn({duration:500, queue:false});
        $('.popup__close' ).on('click', function() {
            $('.register-popup').fadeOut({duration:500, queue:false});
            $('.modal').fadeOut({duration:500, queue:false});
            hideBgModal = true;
            $(this).off('click');
        })
    }

    function showPopupContact(e) {
        $('.contact-popup' ).fadeIn({duration:500, queue:false});
        $('.popup__close' ).on('click', function() {
            $('.contact-popup').fadeOut({duration:500, queue:false});
            $('.modal' ).fadeOut({duration:500, queue:false});
            hideBgModal = true;
            $(this).off('click');
        })
    }

    function showPopupRegisterError(e) {
        $('.register_error-popup' ).fadeIn({duration:500, queue:false});
        $('.popup__close' ).on('click', function() {
            $('.register_error-popup').fadeOut({duration:500, queue:false});
            $('.modal' ).fadeOut({duration:500, queue:false});
            hideBgModal = true;
            $(this).off('click');
        })
    }

    function showPopupRegisterExist(e) {
        $('.register_exist-popup' ).fadeIn({duration:500, queue:false});
        $('.popup__close' ).on('click', function() {
            $('.register_exist-popup').fadeOut({duration:500, queue:false});
            $('.modal' ).fadeOut({duration:500, queue:false});
            hideBgModal = true;
            $(this).off('click');
        })
    }

    // Store
    function filter(e) {
        $('#filter_store').submit();
    }

    //Shopping Cart
    function makeRequest(e) {
        e.preventDefault();
        $.get('/tienda/hacerpedido/' )
            .done(function() {
                showPopupOrder();
            })
            .fail(function() {
                window.location.replace('/micuenta/')
            })
    }

    function showPopupOrder(e) {
        $('.order-popup' ).fadeIn({duration:500, queue:false});
        $('.modal').fadeIn({duration:500, queue:false});
        $('.popup__close' ).on('click', function() {
            $('.order-popup').fadeOut({duration:500, queue:false});
            $('.modal' ).fadeOut({duration:500, queue:false});
            $(this).off('click');
            window.location.replace('/tienda/');
        })
    }

})();
