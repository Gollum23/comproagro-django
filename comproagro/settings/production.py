# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '20/05/15'
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

# INSTALLED_APPS += (
#     'django_extensions',
# )

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ['DB_COMPROAGRO'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS']
    }
}

EMAIL_RECIPENT_LIST_CONTACT = [
    'info@comproagro.com'
]

STATIC_ROOT = '/home/comproagro/webapps/comproagro_static/'