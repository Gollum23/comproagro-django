# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render

from administrator.forms import UserFormEdit, BuyerEditProfileForm
from administrator.models import Buyer


@login_required(login_url=reverse_lazy('website:myaccount'))
def orders_view(request):
    buyer = Buyer.objects.get(user=request.user)
    ctx = {
        'buyer': buyer
    }
    return render(request, 'buyer_orders.html', ctx)


@login_required(login_url=reverse_lazy('website:myaccount'))
def profile_view(request):
    user = request.user
    buyer = Buyer.objects.get(user=user)
    form = UserFormEdit(request.POST or None, instance=user)
    formp = BuyerEditProfileForm(request.POST or None, instance=buyer)
    if form.is_valid():
        user = form.save(commit=False)
        user.save()
        if formp.is_valid():
            profile = formp.save(commit=False)
            profile.user_buyer = user
            profile.save()
            return HttpResponseRedirect(request.path)
    ctx = {
        'form': form,
        'formp': formp
    }
    return render(request, 'buyer_account.html', ctx)
