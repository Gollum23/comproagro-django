# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='shipping_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='sell_unit',
            field=models.CharField(default='kilo', max_length=16, choices=[('kilo', 'Kilo'), ('libra', 'Libra'), ('caja', 'Caja'), ('unidad', 'Unidad'), ('arroba', 'Arroba'), ('bulto', 'Bulto')]),
        ),
    ]
