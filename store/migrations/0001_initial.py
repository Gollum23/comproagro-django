# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import store.models


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
                ('code', models.CharField(max_length=8)),
                ('ship', models.FloatField()),
                ('fee', models.FloatField()),
                ('subtotal', models.FloatField()),
                ('comments', models.TextField()),
                ('state', models.CharField(default='pendiente', max_length=50, choices=[('cancelado', 'Cancelado'), ('enviado', 'Envidado'), ('pendiente', 'Pendiente')])),
                ('shipping_date', models.DateField(blank=True)),
                ('buyer', models.ForeignKey(to='administrator.Buyer')),
                ('producer', models.ForeignKey(blank=True, to='administrator.Producer', null=True)),
            ],
            options={
                'db_table': 'order',
                'verbose_name': 'Pedido',
                'verbose_name_plural': 'Pedidos',
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField()),
                ('subtotal', models.FloatField(blank=True)),
                ('order', models.ForeignKey(to='store.Order')),
            ],
            options={
                'db_table': 'order_item',
                'verbose_name': 'Item pedido',
                'verbose_name_plural': 'Items pedido',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('variety', models.CharField(max_length=255)),
                ('category', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=store.models.upload_product_image)),
                ('sell_unit', models.CharField(default='kilo', max_length=16, choices=[('kilo', 'Kilo'), ('libra', 'Libra'), ('caja', 'Caja'), ('unidad', 'Unidad')])),
                ('min_selling', models.PositiveIntegerField()),
                ('price', models.FloatField()),
                ('features', models.TextField()),
            ],
            options={
                'db_table': 'product',
                'verbose_name': 'Producto',
                'verbose_name_plural': 'Productos',
            },
        ),
        migrations.AddField(
            model_name='orderitem',
            name='product',
            field=models.ForeignKey(to='store.Product'),
        ),
    ]
