# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_auto_20150526_1101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='state',
            field=models.CharField(default='pendiente', max_length=50, choices=[('cancelado', 'Cancelado'), ('en proceso', 'En proceso'), ('enviado', 'Envidado'), ('pendiente', 'Pendiente')]),
        ),
        migrations.AlterField(
            model_name='product',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
