# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from administrator.models import TimeStampModel, Buyer, Producer


SELL_UNIT = (
    ('kilo', 'Kilo'),
    ('libra', 'Libra'),
    ('caja', 'Caja'),
    ('unidad', 'Unidad'),
    ('arroba', 'Arroba'),
    ('bulto', 'Bulto'),
)
STATE_ORDER = (
    ('cancelado', 'Cancelado'),
    ('en proceso', 'En proceso'),
    ('enviado', 'Enviado'),
    ('pendiente', 'Pendiente'),
)


def upload_product_image(request, filename):
    return filename


@python_2_unicode_compatible
class Product(TimeStampModel):

    name = models.CharField(
        max_length=255,
    )
    variety = models.CharField(
        max_length=255
    )
    category = models.CharField(
        max_length=255
    )
    image = models.ImageField(
        upload_to=upload_product_image
    )
    sell_unit = models.CharField(
        max_length=16,
        choices=SELL_UNIT,
        default='kilo'
    )
    min_selling = models.PositiveIntegerField()
    price = models.FloatField()
    features = models.TextField()

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        db_table = 'product'

    def __str__(self):
        return '%s' % self.name


@python_2_unicode_compatible
class Order(TimeStampModel):
    buyer = models.ForeignKey(
        Buyer,
        on_delete=models.PROTECT
    )
    producer = models.ForeignKey(
        Producer,
        null=True,
        blank=True
    )
    code = models.CharField(
        max_length=8
    )
    ship = models.FloatField()
    fee = models.FloatField()
    subtotal = models.FloatField()
    comments = models.TextField()
    state = models.CharField(
        max_length=50,
        choices=STATE_ORDER,
        default='pendiente'
    )
    shipping_date = models.DateField(
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'
        db_table = 'order'

    def __str__(self):
        return '%s' % self.code


@python_2_unicode_compatible
class OrderItem(models.Model):
    order = models.ForeignKey(
        Order
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.PROTECT
    )
    quantity = models.PositiveIntegerField()
    subtotal = models.FloatField(
        blank=True
    )

    class Meta:
        verbose_name = 'Item pedido'
        verbose_name_plural = 'Items pedido'
        db_table = 'order_item'

    def __str__(self):
        return '%s-%s' % (self.order.code, self.product.name)