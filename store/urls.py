# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', store_view, name='home'),
    url(r'^add/$', add_product_car, name='add_to_car'),
    url(r'^delete/(?P<prod>\d+)/$', remove_product_car, name='remove_from_car'),
    url(r'^carrodecompra/$', show_car, name='show_car'),
    url(r'^hacerpedido/$', success_buy, name='make_request'),
    url(r'^detalle-producto/(?P<prod>\d+)/$', product_detail_view, name='product_detail'),
]
