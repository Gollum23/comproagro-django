# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms

from .models import Product, SELL_UNIT


class ProductForm(forms.ModelForm):
    sell_unit = forms.CharField(
        widget=forms.Select(
            choices=SELL_UNIT,
            attrs={
                'class': 'middle'
            }
        )
    )

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Nombre'}),
            'variety': forms.TextInput(attrs={'placeholder': 'Variedad'}),
            'category': forms.TextInput(attrs={'placeholder': 'Categoria'}),
            'image': forms.FileInput(attrs={'class': 'jimage'}),
            'min_selling': forms.TextInput(attrs={
                'placeholder': 'Cantidad mínima', 'class': 'middle'
            }),
            'price': forms.TextInput(attrs={'placeholder': 'Precio'}),
            'features': forms.Textarea(attrs={'placeholder': 'Descripción del producto'})
        }
        labels = {
            'name': 'Nombre',
            'variety': 'Variedad',
            'category': 'Categoria',
            'image': 'Agregar imagen',
            'min_selling': 'Compra mínima',
            'price': 'Precio',
            'features': 'Descripción'
        }


class DeleteProductForm(forms.Form):
    prod = forms.CharField(
        max_length=50
    )
