# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from carton.cart import Cart
from django.template.loader import render_to_string

from administrator.models import UserModel, Buyer
from .models import Product, Order, OrderItem


def store_view(request):
    cart = Cart(request.session)
    list_varieties = Product.objects.values('variety').distinct()
    list_categories = Product.objects.values('category').distinct()

    # Get number items for page
    if request.GET.get('paginate_by') is None:
        items_by_page = 20
    else:
        items_by_page = int(request.GET.get('paginate_by'))

    # Filter for category and variety
    list_filter_category = []
    list_filter_variety = []
    for key, val in request.GET._iterlists():
        if key == 'category':
            for c in val:
                list_filter_category.append(c)
        if key == 'variety':
            for v in val:
                list_filter_variety.append(v)
    list_products = Product.objects.all()
    if list_filter_category:
        list_products = list_products.filter(category__in=list_filter_category)
        list_varieties = Product.objects.filter(category__in=list_filter_category).values('variety').distinct()
    if list_filter_variety:
        list_products = list_products.filter(variety__in=list_filter_variety)
        list_categories = Product.objects.filter(variety__in=list_filter_variety).values('category').distinct()

    # Number unique items in cart
    try:
        number_items = cart.unique_count
    except:
        number_items = 0

    # Order list products
    try:
        order_by = request.GET.get('order_by')
        if order_by == 'pasc':
            list_products = list_products.order_by('name', 'variety')
        elif order_by == 'pdes':
            list_products = list_products.order_by('-name', '-variety')
        else:
            list_products = list_products.order_by('id')
    except KeyError:
        order_by = ''
        list_products = list_products.order_by('id')

    # Pagination
    paginator = Paginator(list_products, items_by_page)
    page = request.GET.get('page')
    try:
        list_products = paginator.page(page)
    except PageNotAnInteger:
        list_products = paginator.page(1)
    except EmptyPage:
        list_products = paginator.page(paginator.num_pages)

    ctx = {
        'list_products': list_products,
        'list_varieties': list_varieties,
        'list_categories': list_categories,
        'list_filter_category': list_filter_category,
        'list_filter_variety': list_filter_variety,
        'number_items': number_items,
        'order_by': order_by,
        'items_by_page': items_by_page
    }

    return render(request, 'store_website.html', ctx)


def add_product_car(request):
    cart = Cart(request.session)
    product_item = Product.objects.get(id=request.GET.get('id'))
    if request.GET.get('quantity'):
        product_quantity = request.GET.get('quantity')
    else:
        product_quantity = product_item.min_selling
    cart.add(product_item, product_item.price, product_quantity)
    return HttpResponseRedirect(reverse_lazy('store:show_car'))

def remove_product_car(request, prod):
    cart = Cart(request.session)
    product_item = Product.objects.get(id=prod)
    cart.remove(product_item)
    return HttpResponseRedirect(reverse_lazy('store:show_car'))

def show_car(request):
    cart = Cart(request.session)
    total = cart.total
    ctx = {
        'total': total
    }
    return render(request, 'shopping_car.html', ctx)

def success_buy(request):
    cart = Cart(request.session)
    if request.user.is_authenticated():
        user = UserModel.objects.get(id=request.user.id, profile='comprador')
        try:
            buyer = Buyer.objects.get(user=user)
        except Buyer.DoesNotExist:
            pass
        order = Order()
        order.buyer = buyer
        order.state = 'pendiente'
        order.code = _generate_code()
        order.fee = 0
        order.ship = 0
        order.subtotal = cart.total
        order.comments = ''
        order.save()
        for item in cart.items:
            orderitem = OrderItem()
            orderitem.order = order
            orderitem.product = item.product
            orderitem.quantity = item.quantity
            orderitem.subtotal = item.subtotal
            orderitem.save()
        subject = 'Pedido ' + order.code
        msg_txt = render_to_string('email/order.txt', {'order': order})
        msg_html = render_to_string('email/order.html', {'order': order})
        list_recipents = settings.EMAIL_RECIPENT_LIST_CONTACT
        list_recipents.append(buyer.user.email)
        send_mail(
            subject,
            msg_txt,
            'info@comproagro.com',
            list_recipents,
            html_message=msg_html
        )
        request.session['CART'] = {}
        return HttpResponse(status=200)
    return HttpResponse(status=403)


def _generate_code():
    import random
    cart_id = ''
    characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    cart_id_length = 8
    for y in range(cart_id_length):
        cart_id += characters[random.randint(0, len(characters) - 1)]
    return cart_id

def product_detail_view(request, prod):
    product = Product.objects.get(pk=prod)
    ctx = {
        'product': product
    }
    return render(request, 'product_detail.html', ctx)


